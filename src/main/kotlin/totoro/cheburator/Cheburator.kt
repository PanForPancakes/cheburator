package totoro.cheburator

fun main() {
    val cfg = Config("cheburator.properties")

    require(cfg.token != null) { "token must be non-null" }

    val ircManager = IrcManager(
        cfg.ircHost,
        cfg.ircChannel,
        cfg.nickname,
        cfg.nickservAccount,
        cfg.nickservPassword,
        cfg.maxLines,
        cfg.sendDelay,
        cfg.segmentStyle
    )
    val discordManager = DiscordManager(cfg.token!!, cfg.discordChannel)
    ircManager.subscribe(discordManager::send)
    discordManager.subscribe(ircManager::send)
}
