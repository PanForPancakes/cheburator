package totoro.cheburator

import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message.Attachment
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.cache.CacheFlag
import net.dv8tion.jda.api.entities.Message as DiscordMessage

class DiscordManager(token: String, private val channel: String) : Manager() {
    private var lastUsers = mutableMapOf<String, String>()

    private val jda = JDABuilder.create(token, listOf(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES))
        .disableCache(
            CacheFlag.ACTIVITY,
            CacheFlag.VOICE_STATE,
            CacheFlag.EMOTE,
            CacheFlag.CLIENT_STATUS,
            CacheFlag.ONLINE_STATUS
        )
        .addEventListeners(object : ListenerAdapter() {
            @Override
            override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
                if (listener != null) {
                    if (event.author.asTag != event.jda.selfUser.asTag && event.channel.name == channel) {
                        val message = event.message
                        updateMemberCache(message)
                        listener?.invoke(convertMessage(message))
                    }
                }
            }
        })
        .setActivity(Activity.watching("IRC"))
        .build()

    private fun updateMemberCache(event: DiscordMessage) {
        val member = event.member ?: return

        lastUsers[member.effectiveName] = member.id
    }

    private fun convertMessage(message: DiscordMessage): Message {
        val name = resolveAuthor(message)
        val text = handleAttachments(message)
        val referencedMessage = message.referencedMessage?.let(::convertMessage)

        return Message.Regular(name, text, referencedMessage)
    }

    override fun send(message: Message) {
        jda.getTextChannelsByName(channel, false).forEach {
            when (message) {
                is Message.Regular -> {
                    val text = resolveMentions(it.guild, message.text)
                    it.sendMessage("**${message.author}:** $text").submit()
                }

                is Message.Action -> {
                    val text = resolveMentions(it.guild, message.action)
                    it.sendMessage("* **${message.author}** $text").submit()
                }

                is Message.Join -> it.sendMessage("**__${message.user}__ has joined the channel.**").submit()

                is Message.Quit -> when (message.reason) {
                    is Message.Quit.Reason.Quit ->
                        it.sendMessage("**__${message.user}__ has quit the server:** ${message.reason.message}.")
                            .submit()

                    is Message.Quit.Reason.Part ->
                        it.sendMessage("**__${message.user}__ has left the channel:** ${message.reason.message}.")
                            .submit()

                    is Message.Quit.Reason.Kick ->
                        it.sendMessage(
                            "**__${message.user}__ has been kicked from the channel " +
                                    "by __${message.reason.actor}__:** ${message.reason.message}"
                        ).submit()
                }
            }
        }
    }

    private fun resolveAuthor(event: DiscordMessage): String {
        val member = event.member

        return member?.effectiveName ?: event.author.name
    }

    // accepts any of the following formats:
    // - @name
    // - @name#discriminator
    // - @[name]
    // - @[name]#discriminator
    // - @[====[name]====]
    // - @[====[name]====]#discriminator
    // the multi-bracketed variants match the opening [====[ with the closest closing multi-bracket ]====]
    // that has the same number of equal signs (possibly zero)
    private val mentionRegex =
        Regex("@(?:([^\\p{Punct}\\s]+)|\\[(?![\\[=])([^]]+)]|\\[(=*)\\[(.+?)]\\3])(?:#(\\d{4}))?")

    private fun resolveMentions(guild: Guild, message: String): String = mentionRegex.replace(message) replace@{
        val name = it.groupValues[1]
            .ifEmpty { it.groupValues[2] }
            .ifEmpty { it.groupValues[4] }
        val discriminator = it.groupValues[5].ifEmpty { null }
        val id = getIdFromName(guild, name, discriminator) ?: return@replace it.value

        // update the cache here as well for convenience
        lastUsers[name] = id

        return@replace "<@$id>"
    }

    private fun getIdByNameAndDiscriminator(guild: Guild, name: String, discriminator: String): String? =
        guild.getMembersByName(name, true).find { it.user.discriminator == discriminator }?.id

    private fun getIdFromName(guild: Guild, name: String, discriminator: String? = null): String? {
        if (discriminator != null) {
            // try searching for username#discriminator
            getIdByNameAndDiscriminator(guild, name, discriminator)?.let { return it }
        }

        // once we're here, the discriminator doesn't matter
        val candidates = guild.getMembersByNickname(name, true).ifEmpty {
            guild.getMembersByName(name, true)
        }

        val disambiguatedId = lastUsers[name].takeIf { candidates.size > 1 }

        return disambiguatedId ?: candidates.firstOrNull()?.id
    }

    private fun handleAttachments(message: DiscordMessage): String {
        val originalMessage = message.contentDisplay
        val attachmentUrls = message.attachments.joinToString(" ") { it.resolvedUrl }

        return if (originalMessage.isEmpty()) {
            attachmentUrls
        } else {
            "$originalMessage $attachmentUrls"
        }
    }

    private val Attachment.resolvedUrl: String
        get() = if (width != -1 && height != -1) proxyUrl else url
}
