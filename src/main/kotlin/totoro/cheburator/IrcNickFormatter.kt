package totoro.cheburator

import org.kitteh.irc.client.library.util.Format
import kotlin.math.absoluteValue

object IrcNickFormatter {
    private val colorArray = listOf(
        Format.DARK_BLUE,
        Format.DARK_GREEN,
        Format.RED,
        Format.PURPLE,
        Format.YELLOW,
        Format.GREEN,
        Format.CYAN,
        Format.MAGENTA,
        45,
        47,
        53,
        56,
        63,
        68,
        87
    )

    private fun calculateHash(nick: String): Int {
        // That plus-one is for the default color
        val modulo = colorArray.size + 1

        return nick.hashCode().absoluteValue % modulo
    }

    fun format(nick: String, bold: Boolean = true): String {
        val hash = calculateHash(nick)
        val boldPrefix = if (bold) Format.BOLD else ""

        val formattedNick = if (hash != 0) {
            // Non-default color
            "${Format.COLOR_CHAR}${colorArray[hash - 1]}$nick"
        } else {
            nick
        }

        return "$boldPrefix$formattedNick"
    }
}