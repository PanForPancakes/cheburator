package totoro.cheburator

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessageCutterTest {
    private val cutter = ByteAwareIrcMessageCutter()

    @Test
    fun `single line messages`() {
        assertEquals(mutableListOf("test"), cutter.split("test", 4))
        assertEquals(mutableListOf("test test"), cutter.split("test test", 9))
        assertEquals(mutableListOf("тест"), cutter.split("тест", 8))
        assertEquals(mutableListOf("teтестst"), cutter.split("teтестst", 12))
        assertEquals(mutableListOf("тесто тесто"), cutter.split("тесто тесто", 21))
        assertEquals(mutableListOf("  test"), cutter.split("  test", 10))
        assertEquals(mutableListOf("привет, мир"), cutter.split("привет, мир" + " ".repeat(40), 20))
    }

    @Test
    fun `multi-line messages`() {
        assertEquals(mutableListOf("привет,", "мир"), cutter.split("привет, мир", 13))
        assertEquals(mutableListOf("привет,", "мир"), cutter.split("привет, мир", 16))
        assertEquals(mutableListOf("привет,", "мир"), cutter.split("привет,      мир", 16))
        assertEquals(mutableListOf("привет,", "   мир"), cutter.split("привет,\n   мир", 100))
    }
}